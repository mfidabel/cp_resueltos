#include <vector>
#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int N;
    while (cin >> N)
    {
        vector<int> numeros(N);
        for (int i = 0; i < N; i++)
            cin >> numeros[i];

        vector<bool> esta(N, false);

        for (int i = 0; i < N - 1; i++)
            esta[abs(numeros[i] - numeros[i + 1])] = true;

        int i;
        for (i=1; i<N; i++){
            if (!esta[i]){
                break;
            }
        }

        cout << (i == N || N == 1 ? "Jolly\n" : "Not jolly\n");
    }

    return 0;
}