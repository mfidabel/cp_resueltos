#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <assert.h> 
#include <iostream>
#include <bitset>

using namespace std;



int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        int N;
        cin >> N;
        vector <long long> enteros(N);
        for (int i=0; i<N ; i++){
            cin >> enteros[i];
        }
        //Analizar
        long long diferenciaMinima = 0;
        int posMayor = 0;
        for (int i=1; i<N; i++){
            if (enteros[i]>enteros[posMayor]) posMayor = i;
            if (enteros[posMayor] - enteros[i] > diferenciaMinima) diferenciaMinima = enteros[posMayor] - enteros[i];
        }
        bitset<64> diferenciaMinimaBits(diferenciaMinima);
        bool encontro = false;
        for (int i=63; i>=0; i--){
            if (diferenciaMinimaBits[i]){
                cout << (i+1) << "\n";
                encontro = true;
                break;
            }
        }

        if (!encontro) cout << "0\n";

    }

    return 0;
}

