#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <assert.h> 
#include <iostream>
#include <vector>

using namespace std;


void swap(int i, int j, vector<vector<int> > &cell){
    int aux = cell[i][i];
    cell[i][i] = cell[j][j];
    cell[j][j] = aux;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        //Hacer aca
        vector<vector<int> > cell(9, vector<int>(9));
        for (int i=0; i<9; i++){
            string linea;
            cin >> linea;
            for (int j=0; j<9; j++){
                cell[i][j] = linea[j] - '0';
            }
        }

        for (int i=0; i<9; i += 3){
            int k = i;
            for (int j=0; j<9; j+=3){
                cell[k][j+i/3] = (cell[k][j+i/3] + 1) > 9 ? 1 : (cell[k][j+i/3] + 1);
                k++;
            }
        }
        

        //Imprimir
        for (int i=0; i<9; i++){
            for (int j=0; j<9; j++){
                cout << cell[i][j];
            }
            cout << "\n";
        }
    }
    return 0;
}

