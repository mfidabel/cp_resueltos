#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <assert.h> 
#include <iostream>

using namespace std;



int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        int N; 
        cin >> N;
        vector<long long> enteros(N);
        for (int i=0; i<N; i++){
            cin >> enteros[i];
        }
        //Resolver
        //ordenar
        sort(enteros.begin(), enteros.end());
        if (N%2==0){
            int mitad = N/2 - 1;
            for (int i=mitad; i>=0; i--){
                cout << enteros[N-i-1] << " " << enteros[i];
                if (i>0) cout << " ";
                else cout << "\n";
            }
        } else {
            //No es a la mitad
            int mitad = N/2;
            cout << enteros[mitad] << " ";
            for (int i=mitad-1; i>=0; i--){
                cout << enteros[N-i-1] << " " << enteros[i];
                if (i>0) cout << " ";
                else cout << "\n";
            }
        }
    }
    return 0;
}

