#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <assert.h> 
#include <iostream>

using namespace std;



int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        //Hacer aca+
        int N, A, B;
        cin >> N >> A >> B;
        string respuesta = "";
        char primeraLetra = 'a';
        if (B == 1){
            for (int i=0; i<N; i++){
                respuesta.push_back('a');
            }
        } else
        {
            for (int i=0; i<N; i++){
                respuesta.push_back(primeraLetra + (i%B));
            }
        }
        

        cout << respuesta << "\n";
    }
    return 0;
}

