#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <assert.h> 
#include <iostream>

using namespace std;



int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        //Hacer aca
        int N;
        cin >> N;
        if (N < 2){
            cout << "0\n";
        } else {
            int mitad = N/2;
            cout << (N%2==0 ? mitad - 1 : mitad) << "\n"; 
        }
    }
    return 0;
}

