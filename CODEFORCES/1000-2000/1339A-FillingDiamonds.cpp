#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <assert.h> 
#include <iostream>

using namespace std;



int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        int N;
        cin >> N;
        cout << N << "\n";
    }
    return 0;
}

