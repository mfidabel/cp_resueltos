#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <assert.h> 
#include <iostream>
#include <vector>

using namespace std;

int min(int a, int b){
    return a < b ? a : b;
}

int max(int a, int b){
    return a > b ? a : b;
}


int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--){
        //Hacer aca
        int N;
        cin >> N;
        vector<int> estudiantes(N+1, 0);
        for (int i=1; i<=N; i++){
            int aux;
            cin >> aux;
            estudiantes[aux]++;
        }
        //Resolver
        int diferentes = 0;
        int posMayor = 1;
        for (int i=1; i<=N; i++){
            if (estudiantes[i]>0) diferentes++;
            if (estudiantes[i] > estudiantes[posMayor]) posMayor = i;
        }

        int respuesta = min(diferentes < estudiantes[posMayor] ? diferentes : diferentes -1, min(N/2, estudiantes[posMayor]));

        cout << respuesta << "\n";

    }
    return 0;
}

