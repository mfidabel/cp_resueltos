#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <bitset>

using namespace std;
vector<long long> sumaFila(32);

void solucion()
{
    //Hacer programa aca
    unsigned long long sum = 0, N;
    int r = 0;
    cin >> N;
    if (N < 100)
    {
        for (int i=1; i<=N; i++){
            cout << i << " 1\n";
        }
    }
    else
    {
        //Trataremos de llegar a N-32 para cubrir los saltos, y luego lo que falta llenamos con 1
        bitset<64> x(N - 32);
        bool izq = true;
        for (int i = 0; i < 64; i++)
        {
            if (x[r])
            {
                //Bit es 1, imprimir toda la fila
                sum += sumaFila[r];
                if (izq)
                {
                    //De izquierda a derecha
                    for (int k = 1; k <= r + 1; k++)
                    {
                        cout << (r + 1) << " " << k << "\n";
                    }
                }
                else
                {
                    //De derecha a izquierda
                    for (int k = r + 1; k >= 1; k--)
                    {
                        cout << (r + 1) << " " << k << "\n";
                    }
                }

                izq = !izq;
            }
            else
            {
                //Bit es 0
                if (sum < N)
                {
                    cout << (r + 1) << " " << (izq ? 1 : (r + 1)) << "\n";
                    sum++;
                }
                else
                {
                    break;
                }
            }
            r++;
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    sumaFila[0] = 1;
    for (int i = 1; i < 32; i++)
    {
        sumaFila[i] = sumaFila[i - 1] * 2;
    }
    int T;
    cin >> T;
    for (int caso = 1; caso <= T; caso++)
    {
        cout << "Case #" << caso << ": \n";
        solucion();
    }

    return 0;
}
