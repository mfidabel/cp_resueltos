#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>

using namespace std;

bool contieneSufijo(string palabra, string sufijo)
{
    int n = palabra.size(), m = sufijo.size();
    if (n == 0 || m == 0)
        return true; //Vacio es sufijo de cualquier cadena
    if (m > n)
        return false; //Por si acaso, no creo que se de

    bool esSufijo = true;
    for (int i = n-1, j = m-1; i >= 0 && j >= 0 && esSufijo; i--, j--){
        esSufijo = palabra[i] == sufijo[j];
    }
    return esSufijo;
}

bool contienePrefijo(string palabra, string prefijo)
{
    int n = palabra.size(), m = prefijo.size();
    if (n == 0 || m == 0)
        return true; //Vacio es prefijo de cualquier cadena
    if (m > n)
        return false; //Por si acaso, no creo que se de

    bool esPrefijo = true;
    for (int i = 0, j = 0; i < n && j < m && esPrefijo; i++, j++){
        esPrefijo = palabra[i] == prefijo[j];
    }
    return esPrefijo;
}

void procesar(const string palabra, vector<string> &prefijos, vector<string> &sufijos, vector<string> &medio){
    vector<string> aux;
    size_t desde = 0;
    for (size_t hasta=palabra.find_first_of('*'); hasta != string::npos; hasta = palabra.find_first_of('*', desde)){
        aux.push_back(palabra.substr(desde, hasta-desde));
        desde = hasta + 1;
    }
    aux.push_back(palabra.substr(desde, string::npos));

    int num_tokens = aux.size();
    prefijos.push_back(aux[0]);
    sufijos.push_back(aux[num_tokens-1]);
    string medioAux = "";
    for (int i=1; i<num_tokens-1; i++){
        medioAux += aux[i];
    }
    medio.push_back(medioAux);
}


void solucion()
{
    //Hacer programa aca
    int N;
    cin >> N;
    vector<string> patrones(N), prefijos, sufijos, medios;
    //Leemos
    for (int i = 0; i < N; i++)
    {
        cin >> patrones[i];
        procesar(patrones[i], prefijos, sufijos, medios);
    }

    //Buscar mayor prefijo
    int mayorPrefijo = 0;
    for (int i=0; i<N; i++){
        if (prefijos[i].size() > prefijos[mayorPrefijo].size()) mayorPrefijo = i;
    }

    //Buscar mayor sufijo
    int mayorSufijo = 0;
    for (int i=0; i<N; i++){
        if (sufijos[i].size() > sufijos[mayorSufijo].size()) mayorSufijo = i;
    }

    //Verificar que todos son prefijos
    for (int i=0; i<N; i++){
        if (!contienePrefijo(prefijos[mayorPrefijo], prefijos[i])){
            //No es prefijo
            cout << "*\n";
            return;
        }
    }

    //Verificar que todos son sufijos
    for (int i=0; i<N; i++){
        if (!contieneSufijo(sufijos[mayorSufijo], sufijos[i])){
            //No es sufijo
            cout << "*\n";
            return;
        }
    }

    /*
    cout << prefijos[mayorPrefijo];

    for (int i=0; i<N; i++){
        for (int j=0; j<medios[i])
    }
    */
    //Imprimir todo
    string resultado = prefijos[mayorPrefijo];
    for (int i=0; i<N; i++){
        resultado += medios[i];
    }
    resultado += sufijos[mayorSufijo];

    cout << resultado << "\n";
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    for (int caso = 1; caso <= T; caso++)
    {
        cout << "Case #" << caso << ": ";
        solucion();
    }

    return 0;
}
